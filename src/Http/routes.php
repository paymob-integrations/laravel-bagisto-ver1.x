<?php

Route::group([
    //   'prefix'     => 'paymob',
       'middleware' => ['web', 'theme', 'locale', 'currency']
   ], function () {
       Route::get('redirect','Bagisto\Paymob\Http\Controllers\PaymobController@redirect')->name('paymob.process');
       Route::any('paymob/callback','Bagisto\Paymob\Http\Controllers\PaymobController@callback')->name('paymob.callback'); 
});