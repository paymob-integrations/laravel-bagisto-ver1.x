<?php

return [
    'admin' => [
        'system' => [
            'public-key' => 'Public Key',
            'secret-key' => 'Secret Key',
            'integration-ids' => 'Payment method(s)',
            'hmac' => 'HMAC',
            'paymobstatus' => 'Status',
            'callback-url' => 'Callback URL',
            'debug-log' => 'Debug Log',
            'sort-order' => 'Sort Order'
        ],
    ]
];  