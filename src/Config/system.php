<?php

return [
    [
        'key'    => 'sales.paymentmethods.paymob',
        'name'   => 'Paymob',
        'sort'   => 4,
        'fields' => [
            [
                'name'          => 'title',
                'title'         => 'admin::app.admin.system.title',
                'type'          => 'depends',
                'depend'        => 'active:1',
                'validation'    => 'required_if:active,1',
                'channel_based' => false,
                'locale_based'  => true,
            ], [
                'name'          => 'description',
                'title'         => 'admin::app.admin.system.description',
                'type'          => 'textarea',
                'channel_based' => false,
                'locale_based'  => true,
            ],
			[
                'name'          => 'public-key',
                'title'         => 'paymob::app.admin.system.public-key',
                'type'          => 'depends',
                'depend'        => 'active:1',
                'validation'    => 'required_if:active,1',
                'channel_based' => false,
                'locale_based'  => true,
            ],	
            [
                'name'          => 'secret-key',
                'title'         => 'paymob::app.admin.system.secret-key',
                'type'          => 'depends',
                'depend'        => 'active:1',
                'validation'    => 'required_if:active,1',
                'channel_based' => false,
                'locale_based'  => true,
            ],
			[
                'name'          => 'integration-ids',
                'title'         => 'paymob::app.admin.system.integration-ids',
                'type'          => 'depends',
                'depend'        => 'active:1',
                'validation'    => 'required_if:active,1',
                'channel_based' => false,
                'locale_based'  => true,
                'info'          => 'Add the Payment methods ID(s) that exist in Paymob account separated by comma , separator. (Example: 123456,98765,45678)'
            ],
            [
                'name'          => 'hmac',
                'title'         => 'paymob::app.admin.system.hmac',
                'type'          => 'depends',
                'depend'        => 'active:1',
                'validation'    => 'required_if:active,1',
                'channel_based' => false,
                'locale_based'  => true
            ],
            [
                'name'          => 'paymobcallback',
                'title'         => 'paymob::app.admin.system.callback-url',
                'type'          => 'hidden',
                'channel_based' => false,
                'locale_based'  => true,
                'info'          => ' Transaction Processed/Response Callback URL: '.config('app.url').'/paymob/callback ',
            ],
            [
                'name'          => 'debug-log',
                'title'         => 'paymob::app.admin.system.debug-log',
                'type'          => 'boolean',
                'channel_based' => false,
                'locale_based'  => true,
                'info'          =>'Log file will be saved in '.storage_path() .'/logs/'
            ],
			[
                'name'          => 'active',
                'title'         => 'paymob::app.admin.system.paymobstatus',
                'type'          => 'boolean',
                'validation'    => 'required',
                'channel_based' => false,
                'locale_based'  => true
            ],
            [
                'name'    => 'sort',
                'title'   => 'paymob::app.admin.system.sort-order',
                'type'    => 'select',
                'options' => [
                    [
                        'title' => '1',
                        'value' => 1,
                    ], [
                        'title' => '2',
                        'value' => 2,
                    ], [
                        'title' => '3',
                        'value' => 3,
                    ], [
                        'title' => '4',
                        'value' => 4,
                    ], [
                        'title' => '5',
                        'value' => 5,
                    ],
                ],
            ],
				
        ]
    ]
];