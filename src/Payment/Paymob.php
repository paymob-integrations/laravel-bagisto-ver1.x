<?php

namespace Bagisto\Paymob\Payment;

use Webkul\Payment\Payment\Payment;

class Paymob extends Payment
{
    /**
     * Payment method code
     *
     * @var string
     */
    protected $code  = 'paymob';

    public function getRedirectUrl()
    {
        return route('paymob.process');
    }
}
